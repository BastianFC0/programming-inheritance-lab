public class Book{
    protected String title;
    private String author;
    public Book(String title, String author){
        this.title = title;
        this.author = author;
    }
    public String getTitle(){
        return this.title;
    }
    public String getAuthor(){
        return this.author;
    }
    public String toString(){
        return "The title of this book is "+this.title+" and the author of the book is "+this.author;
    }
}